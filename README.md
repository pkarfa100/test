apiVersion: v1
data:
  .dockerconfigjson: >-
    eyJhdXRocyI6eyJodHRwczovL3BvY2RvY2tlcnJlZ2lzdHJ5MS5henVyZWNyLmlvIjp7InVzZXJuYW1lIjoicG9jZG9ja2VycmVnaXN0cnkxIiwicGFzc3dvcmQiOiJaM1hOdW5iVVZkNlVTcndrK1RGbUh5aVZkTXIvZzVkYiIsImF1dGgiOiJjRzlqWkc5amEyVnljbVZuYVhOMGNua3hPbG96V0U1MWJtSlZWbVEyVlZOeWQyc3JWRVp0U0hscFZtUk5jaTluTldSaSJ9fX0=
kind: Secret
metadata:
  annotations:
    strategy.spinnaker.io/versioned: 'false'
  name: acr-secret
type: kubernetes.io/dockerconfigjson
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: dep-database
spec:
  replicas: 1
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 1
  selector:
    matchLabels:
      app: database
  template:
    metadata:
      annotations:
        sidecar.istio.io/rewriteAppHTTPProbers: 'true'
      labels:
        app: database
        pod: database
    spec:
      containers:
        - env:
            - name: MYSQL_DATABASE
              value: ihgdemo
            - name: MYSQL_USER
              value: ihguser
            - name: MYSQL_PASSWORD
              value: ihgpassword
            - name: MYSQL_ROOT_PASSWORD
              value: ihgpassword
          image: 'pocdockerregistry1.azurecr.io/poc/mysql-database:latest'
          name: database
          ports:
            - containerPort: 3306
      imagePullSecrets:
        - name: acr-secret
---
  apiVersion: v1
  kind: Service
  metadata:
    name: svc-database
  spec:
    ports:
      - name: databse
        port: 3306
        targetPort: 3306
    selector:
      pod: database
    type: ClusterIP
---
apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
    strategy.spinnaker.io/max-version-history: '2'
  labels:
    dep: frontend
  name: dep-frontend
spec:
  replicas: 1
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 1
  selector:
    matchLabels:
      app: frontend
  template:
    metadata:
      annotations:
        sidecar.istio.io/rewriteAppHTTPProbers: 'true'
      labels:
        app: frontend
        buildNo: 'tbd'
        pod: frontend
        triggerBy: 'tbd'
    spec:
      containers:
        - env:
            - name: SEARCH_SERVICE
              value: 'http://svc-property-search:8303'
          image: >-
            pocdockerregistry1.azurecr.io/poc/property-search-service-webapp:latest
          livenessProbe:
            httpGet:
              httpHeaders:
                - name: Cookie
                  value: shop_session-id=x-liveness-probe
              path: /
              port: 30100
            initialDelaySeconds: 10
          name: server
          ports:
            - containerPort: 30100
          readinessProbe:
            httpGet:
              httpHeaders:
                - name: Cookie
                  value: shop_session-id=x-readiness-probe
              path: /
              port: 30100
            initialDelaySeconds: 10
          resources:
            limits:
              cpu: 100m
            requests:
              cpu: 10m
      imagePullSecrets:
        - name: acr-secret
      initContainers:
        - command:
            - sh
            - '-c'
            - >-
              until nslookup svc-property-search; do echo waiting for property
              search service; sleep 2; done;
          image: busybox
          name: init-db
      terminationGracePeriodSeconds: 5
---
apiVersion: v1
kind: Service
metadata:
  labels:
    svc: frontend
  name: svc-frontend
spec:
  ports:
    - name: http
      port: 80
      targetPort: 30100
  selector:
    pod: frontend
  type: LoadBalancer
---
apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
    strategy.spinnaker.io/max-version-history: '2'
  labels:
    dep: property-search
  name: dep-property-search
spec:
  replicas: 1
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 1
  selector:
    matchLabels:
      app: property-search-service
  template:
    metadata:
      labels:
        app: property-search-service
        buildNo: 'tbd'
        pod: property-search-service
        triggerBy: 'tbd'
    spec:
      containers:
        - env:
            - name: DETAILS_SERVICE_HOST
              value: 'http://svc-hotel-details:8300'
            - name: INVENTORY_SERVICE_HOST
              value: 'http://svc-inventory:8301'
            - name: PRICE_SERVICE_HOST
              value: 'http://svc-price:8302'
          image: >-
            pocdockerregistry1.azurecr.io/poc/property-search-service:latest
          livenessProbe:
            failureThreshold: 10
            httpGet:
              path: /actuator/health
              port: 8303
            initialDelaySeconds: 120
            periodSeconds: 15
            timeoutSeconds: 10
          name: server
          ports:
            - containerPort: 8303
          readinessProbe:
            failureThreshold: 10
            httpGet:
              path: /actuator/health
              port: 8303
            initialDelaySeconds: 120
            periodSeconds: 15
            timeoutSeconds: 10
          resources:
            limits:
              cpu: 500m
            requests:
              cpu: 10m
      imagePullSecrets:
        - name: acr-secret
      initContainers:
        - command:
            - sh
            - '-c'
            - >-
              until nslookup svc-database; do echo waiting for mysql service;
              sleep 2; done;
          image: busybox
          name: init-db
      terminationGracePeriodSeconds: 5
---
apiVersion: v1
kind: Service
metadata:
  labels:
    svc: property-search
  name: svc-property-search
spec:
  ports:
    - name: service
      port: 8303
      targetPort: 8303
  selector:
    pod: property-search-service
  type: ClusterIP
---
apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
    strategy.spinnaker.io/max-version-history: '2'
  labels:
    dep: hotel-details
  name: dep-hotel-details
spec:
  replicas: 1
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 1
  selector:
    matchLabels:
      app: hotel-details-service
  template:
    metadata:
      labels:
        app: hotel-details-service
        buildNo: 'tbd'
        pod: hotel-details-service
        triggerBy: 'tbd'
    spec:
      containers:
        - env:
            - name: MYSQL_HOST
              value: svc-database
            - name: MYSQL_DATABASE
              value: ihgdemo
            - name: MYSQL_USER
              value: ihguser
            - name: MYSQL_PASSWORD
              value: ihgpassword
          image: >-
            pocdockerregistry1.azurecr.io/poc/hotel-details-service:latest
          livenessProbe:
            failureThreshold: 10
            httpGet:
              path: /actuator/health
              port: 8300
            initialDelaySeconds: 120
            periodSeconds: 15
            timeoutSeconds: 10
          name: server
          ports:
            - containerPort: 8300
          readinessProbe:
            failureThreshold: 10
            httpGet:
              path: /actuator/health
              port: 8300
            initialDelaySeconds: 120
            periodSeconds: 15
            timeoutSeconds: 10
          resources:
            limits:
              cpu: 300m
            requests:
              cpu: 10m
      imagePullSecrets:
        - name: acr-secret
      initContainers:
        - command:
            - sh
            - '-c'
            - >-
              until nslookup svc-database; do echo waiting for mysql service;
              sleep 2; done;
          image: busybox
          name: init-db
      terminationGracePeriodSeconds: 5
---
apiVersion: v1
kind: Service
metadata:
  labels:
    svc: hotel-details
  name: svc-hotel-details
spec:
  ports:
    - name: service
      port: 8300
      targetPort: 8300
  selector:
    pod: hotel-details-service
  type: ClusterIP
---
apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
    strategy.spinnaker.io/max-version-history: '2'
  labels:
    dep: inventory
  name: dep-inventory
spec:
  replicas: 1
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 1
  selector:
    matchLabels:
      app: inventory-service
  template:
    metadata:
      labels:
        app: inventory-service
        buildNo: 'tbd'
        pod: inventory-service
        triggerBy: 'tbd'
    spec:
      containers:
        - env:
            - name: MYSQL_HOST
              value: svc-database
            - name: MYSQL_DATABASE
              value: ihgdemo
            - name: MYSQL_USER
              value: ihguser
            - name: MYSQL_PASSWORD
              value: ihgpassword
          image: >-
            pocdockerregistry1.azurecr.io/poc/inventory-service:latest
          livenessProbe:
            failureThreshold: 10
            httpGet:
              path: /actuator/health
              port: 8301
            initialDelaySeconds: 120
            periodSeconds: 15
            timeoutSeconds: 10
          name: server
          ports:
            - containerPort: 8301
          readinessProbe:
            failureThreshold: 10
            httpGet:
              path: /actuator/health
              port: 8301
            initialDelaySeconds: 120
            periodSeconds: 15
            timeoutSeconds: 10
          resources:
            limits:
              cpu: 350m
            requests:
              cpu: 10m
      imagePullSecrets:
        - name: acr-secret
      initContainers:
        - command:
            - sh
            - '-c'
            - >-
              until nslookup svc-database; do echo waiting for mysql service;
              sleep 2; done;
          image: busybox
          name: init-db
      terminationGracePeriodSeconds: 5
---
apiVersion: v1
kind: Service
metadata:
  labels:
    svc: inventory
  name: svc-inventory
spec:
  ports:
    - name: service
      port: 8301
      targetPort: 8301
  selector:
    pod: inventory-service
  type: ClusterIP
---
apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
    strategy.spinnaker.io/max-version-history: '2'
  labels:
    dep: price
  name: dep-price
spec:
  replicas: 1
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 1
  selector:
    matchLabels:
      app: price-service
  template:
    metadata:
      labels:
        app: price-service
        buildNo: 'tbd'
        pod: price-service
        triggerBy: 'tbd'
    spec:
      containers:
        - env:
            - name: MYSQL_HOST
              value: svc-database
            - name: MYSQL_DATABASE
              value: ihgdemo
            - name: MYSQL_USER
              value: ihguser
            - name: MYSQL_PASSWORD
              value: ihgpassword
          image: 'pocdockerregistry1.azurecr.io/poc/price-service:latest'
          livenessProbe:
            failureThreshold: 10
            httpGet:
              path: /actuator/health
              port: 8302
            initialDelaySeconds: 120
            periodSeconds: 15
            timeoutSeconds: 10
          name: server
          ports:
            - containerPort: 8302
          readinessProbe:
            failureThreshold: 10
            httpGet:
              path: /actuator/health
              port: 8302
            initialDelaySeconds: 120
            periodSeconds: 15
            timeoutSeconds: 10
          resources:
            limits:
              cpu: 300m
            requests:
              cpu: 10m
      imagePullSecrets:
        - name: acr-secret
      initContainers:
        - command:
            - sh
            - '-c'
            - >-
              until nslookup svc-database; do echo waiting for mysql service;
              sleep 2; done;
          image: busybox
          name: init-db
      terminationGracePeriodSeconds: 5
---
apiVersion: v1
kind: Service
metadata:
  labels:
    svc: price
  name: svc-price
spec:
  ports:
    - name: service
      port: 8302
      targetPort: 8302
  selector:
    pod: price-service
  type: ClusterIP
---
apiVersion: autoscaling/v2beta2
kind: HorizontalPodAutoscaler
metadata:
  name: hpa-inventory
spec:
  maxReplicas: 1
  metrics:
    - resource:
        name: cpu
        target:
          averageUtilization: 85
          type: Utilization
      type: Resource
  minReplicas: 1
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: dep-inventory
